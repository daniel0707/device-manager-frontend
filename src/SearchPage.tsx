import React, { FC, useState, FormEvent } from 'react'
import axios from 'axios'
import { Button, Form, Table, Alert } from 'react-bootstrap'
import { Device } from './types'

const SearchPage: FC = () => {
  const [show, setShow] = useState(false)
  const [id, setId] = useState<string>("")
  const [device, setDevice] = useState<Device>()
  
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    try {
      event.preventDefault()
      const resp = await axios.get(`/deviceapi/${id}`)
      setDevice(resp.data)
      setId("")
    } catch (e) {
      setDevice(undefined)
      setId("")
      setShow(true)
      setTimeout(() => setShow(false), 2000)
    }
  }

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Form.Label>Find device</Form.Label>
        <Form.Control
          type="number"
          placeholder="Equipment Number"
          onChange={({ target: { value } }) => setId(value)}
          value={id}
        />
        <Button type="submit">Search</Button>
      </Form>
      {device?
        <Table>
          <thead>
            <tr>
              <th>Equipment Number</th>
              <th>Address</th>
              <th>Contract Start</th>
              <th>Contract End</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <tr >
              <td>{device.EquipmentNumber}</td>
              <td>{device.Address}</td>
              <td>{device.ContractStartDate}</td>
              <td>{device.ContractEndDate}</td>
              <td>{device.Status ? "online" : "offline"}</td>
            </tr>
          </tbody>
        </Table>
        :
        <Alert variant="danger" show={show}>
          Device not found.
        </Alert>
      }
    </div>
  )
}

export default SearchPage