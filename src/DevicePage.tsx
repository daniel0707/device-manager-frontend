import React, {FC, useState, useEffect, useRef} from 'react'
import axios from 'axios'
import { Form, Table } from 'react-bootstrap'
import { Device } from './types'

const DevicePage: FC = () => {
  const [devices, setDevices] = useState(Array<Device>())
  const [limit, setLimit] = useState(50)

  const fetchDevices = async (l: string) => {
    try {
      const resp = await axios.get(`/deviceapi/search?limit=${l}`)
      setDevices(resp.data)
    } catch (e) {
      console.error(e)
    }
  }

  useEffect(() => {
    void fetchDevices(`${limit}`)
  },[])

  function useDebouncedCallback<A extends any[]>(
    callback: (...args: A) => void,
    wait: number
  ) {
    // track args & timeout handle between calls
    const argsRef = useRef<A>();
    const timeout = useRef<ReturnType<typeof setTimeout>>();

    function cleanup() {
      if (timeout.current) {
        clearTimeout(timeout.current);
      }
    }
    // make sure our timeout gets cleared if
    // our consuming component gets unmounted
    useEffect(() => cleanup, []);

    return function debouncedCallback(
      ...args: A
    ) {
      // capture latest args
      argsRef.current = args;

      // clear debounce timer
      cleanup();

      // start waiting again
      timeout.current = setTimeout(() => {
        if (argsRef.current) {
          callback(...argsRef.current);
        }
      }, wait);
    };
  }

  const debouncedFetchDevices = useDebouncedCallback((l) => {
    void fetchDevices(l)
  },300)

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(Number(e.target.value))
    debouncedFetchDevices(e.target.value)
  }
  return (
    <div>
      <Form>
        <Form.Group controlId="formBasicRange">
          <Form.Label>Limit to {limit} devices</Form.Label>
          <Form.Control
            min={1}
            onChange={changeHandler}
            type="range" />
        </Form.Group>
      </Form>
      <Table striped bordered>
        <thead>
          <tr>
            <th>Equipment Number</th>
            <th>Address</th>
            <th>Contract Start</th>
            <th>Contract End</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {devices.map((device) => (
              <tr key={device.EquipmentNumber}>
                <td>{device.EquipmentNumber}</td>
                <td>{device.Address}</td>
                <td>{device.ContractStartDate}</td>
                <td>{device.ContractEndDate}</td>
                <td>{device.Status?"online":"offline"}</td>
              </tr>
            )
          )}
        </tbody>
      </Table>
    </div>
  )
}

export default DevicePage