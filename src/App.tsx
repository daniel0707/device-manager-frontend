/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React, { ReactElement, useState } from 'react';
import { BrowserRouter, Route, Link, Switch, Redirect } from 'react-router-dom'
import { Container, Navbar, Nav } from 'react-bootstrap';
import DevicePage from './DevicePage';
import SearchPage from './SearchPage';
import CreatePage from './CreatePage';

function App(): ReactElement{
  return (
    <div className="App">
      <BrowserRouter>
        <Container>
          <Navbar collapseOnSelect={true} variant="dark" expand="lg" bg="dark">
            <Navbar.Brand>
              KONE device AWS mock app
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link eventKey="1" as={Link} to="/devices">
                    Devices
                </Nav.Link>
                <Nav.Link eventKey="2" as={Link} to="/search">
                    Search
                </Nav.Link>
                <Nav.Link eventKey="3" as={Link} to="/create">
                    Create
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Switch>
            <Route path="/devices" render={() => <DevicePage/>}/>
             <Route path="/search" render={() => <SearchPage/>}/>
            <Route path="/create" render={() => <CreatePage />} />
            <Route exact path="/"><Redirect to="/devices"></Redirect></Route>
          </Switch>
        </Container>
      </BrowserRouter>
    </div>
  );
}

export default App;
