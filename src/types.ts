export interface Device {
  EquipmentNumber: number;
  Address: string;
  ContractEndDate: string;
  Status: boolean;
  ContractStartDate: string;
}