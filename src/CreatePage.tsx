import axios from 'axios'
import React, {FC, useState} from 'react'
import { Form, Button, Col, Container, Row, Alert} from 'react-bootstrap'

const CreatePage: FC = () => {
  const [eq,setEq] = useState("")
  const [status, setStatus] = useState(1)
  const [address, setAddress] = useState("")
  const [startDate, setStartDate] = useState("")
  const [endDate, setEndDate] = useState("")
  const [alert, setAlert] = useState({show: false, state:"success", body:""})


  const submitHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    try {
      event.preventDefault()
      await axios.post(`/deviceapi`, {
        EquipmentNumber: eq,
        Address: address,
        ContractEndDate: endDate,
        Status: status,
        ContractStartDate: startDate
      })
      setAlert({ show: true, state: "success", body: "Device created" })
      setTimeout(() => setAlert({ ...alert, show: false }), 2000)
      setEq("")
      setStatus(1)
      setStartDate("")
      setEndDate("")
      setAddress("")
    } catch (e) { 
      setAlert({ show: true, state: "danger", body: "Failed to create device" })
      setTimeout(() => setAlert({ ...alert, show: false }), 2000)
    }
  }


  return (
    <div>
      <Container fluid="sm">
      <Form onSubmit={submitHandler}>
        <Form.Row>
          <Form.Group as={Col} controlId="formGridEq">
            <Form.Label>Equipment Number</Form.Label>
              <Form.Control required type="number" placeholder="Enter Equipment Number" value={eq} onChange={(e)=> setEq(e.target.value)}/>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridStatus">
                <Form.Label as="legend" column sm={2}>
                Status
                </Form.Label>
              <Col>
                <Form.Check
                  inline
                  type="radio"
                  label="online"
                  name="formHorizontalRadios"
                  id="formHorizontalRadios1"
                  onChange={(e) => setStatus(1)}
                  checked={Boolean(status)}
                  />
                <Form.Check
                  inline
                  type="radio"
                  label="offline"
                  name="formHorizontalRadios"
                  id="formHorizontalRadios2"
                  onChange={(e) => setStatus(0)}
                  checked={!status}
                />
              </Col>
            </Form.Group>
        </Form.Row>

        <Form.Group controlId="formGridAddress">
          <Form.Label>Address</Form.Label>
            <Form.Control required placeholder="1234 Main St" value={address} onChange={(e) => setAddress(e.target.value)}/>
        </Form.Group>

        <Form.Row>
          <Form.Group as={Col} controlId="formGridStartDate">
            <Form.Label>Contract Start Date</Form.Label>
              <Form.Control required type="datetime-local" placeholder="YYYY-MM-DDTHH:mm:ss.000Z" value={startDate} onChange={(e) => setStartDate(e.target.value)}/>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridEndDate">
              <Form.Label>Contract End Date</Form.Label>
              <Form.Control required type="datetime-local" placeholder="YYYY-MM-DDTHH:mm:ss.000Z" value={endDate} onChange={(e) => setEndDate(e.target.value)}/>
          </Form.Group>
        </Form.Row>

        <Button variant="primary" type="submit">
          Submit
        </Button>
        </Form>
        <Alert className="m-1" variant={alert.state} show={alert.show}>{alert.body}</Alert>
      </Container>
    </div>
  )
}

export default CreatePage